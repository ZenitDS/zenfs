#!/bin/ksh

# File consistency checks with some file movement
# in hope that some issues are catched, fingers crossed.

FILE=$(mktemp)
FILE1=$(mktemp);
FILE2=$(mktemp);
FILE3=$(mktemp);
FILE4=$(mktemp);
FILE1_TMP=$(mktemp);
FILE2_TMP=$(mktemp);
FILE3_TMP=$(mktemp);
FILE4_TMP=$(mktemp);
FILE5_TMP=$(mktemp);

dd if=/dev/zero of=$FILE bs=512 count=16388
dd if=/dev/random of=$FILE1 bs=512 count=1024
dd if=/dev/random of=$FILE2 bs=512 count=2047
dd if=/dev/random of=$FILE3 bs=512 count=1024
dd if=/dev/random of=$FILE4 bs=512 count=4097


./newfs/newfs_zenfs -v $FILE

./util/util_zenfs $FILE oicp $FILE1 /file1
./util/util_zenfs $FILE oicp $FILE2 /file2
./util/util_zenfs $FILE oicp $FILE3 /file3

./util/util_zenfs $FILE unlink /file2
./util/util_zenfs $FILE unlink /file1

./util/util_zenfs $FILE oicp $FILE4 /file4

./util/util_zenfs $FILE cp /file4 /file5
./util/util_zenfs $FILE unlink /file4
./util/util_zenfs $FILE cp /file5 /file4

./util/util_zenfs $FILE oicp $FILE2 /file1
./util/util_zenfs $FILE oicp $FILE1 /file2

./util/util_zenfs $FILE iocp /file1 $FILE1_TMP
./util/util_zenfs $FILE iocp /file2 $FILE2_TMP
./util/util_zenfs $FILE iocp /file3 $FILE3_TMP
./util/util_zenfs $FILE iocp /file4 $FILE4_TMP
./util/util_zenfs $FILE iocp /file5 $FILE5_TMP

./util/util_zenfs $FILE ls

ERR=0

cmp -s $FILE2 $FILE1_TMP || ERR=1
cmp -s $FILE1 $FILE2_TMP || ERR=1
cmp -s $FILE3 $FILE3_TMP || ERR=1
cmp -s $FILE4 $FILE4_TMP || ERR=1
cmp -s $FILE4 $FILE5_TMP || ERR=1

if [[ $ERR == 0 ]]
then
	echo "Files were consistent. The world can live one more day"
	echo "[SUCCESS]"
else
	echo "[FAILED]"
fi

rm	$FILE $FILE1 $FILE2 $FILE3 $FILE4 \
	$FILE1_TMP $FILE2_TMP $FILE3_TMP $FILE4_TMP
