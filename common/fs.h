#ifndef _COMMON_FS_H 
#define _COMMON_FS_H

/*
 * fssetup and fsexit load and unload the filesystem state.
 * i.e. file descriptors and header info
 */
int fssetup(const char *const);
int fsexit(void);
/*
 * integer division rounding up
 */
int idivup(int, int);
int check_index(inodei_t);
int ensure_ilength(inodei_t, uint16_t);
int find_free_inode(inodei_t*);
int find_sector_chain(uint32_t*, uint16_t);
int fsrealpath(const char*, char*);
int inode_file_len(inodei_t, size_t*);
int mark_inode(inodei_t, int);
int mark_sectors(uint32_t, uint16_t, int);
int read_idata(inodei_t, void*, size_t, off_t);
int read_inode(inodei_t, fs_inode_t*);
int resolve_inode(inodei_t*, const char*);
int write_idata(inodei_t, void*, size_t, off_t);
int write_inode(inodei_t, fs_inode_t);

#endif
