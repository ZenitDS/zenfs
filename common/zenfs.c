#include "zenfs.h"

#include <memory.h>

pack_fs_head_t
pack_head(fs_head_t unpacked)
{
	pack_fs_head_t packed;
	memcpy(&packed.ident,     &unpacked.ident,     sizeof(packed.ident));
	memcpy(&packed.sectors,   &unpacked.sectors,   sizeof(packed.sectors));
	memcpy(&packed.sreserved, &unpacked.sreserved, sizeof(packed.sreserved));
	memcpy(&packed.sdbmp,     &unpacked.sdbmp,     sizeof(packed.sdbmp));
	memcpy(&packed.sibmp,     &unpacked.sibmp,     sizeof(packed.sibmp));
	memcpy(&packed.sinodes,   &unpacked.sinodes,   sizeof(packed.sinodes));
	return packed;
}

fs_head_t
unpack_head(pack_fs_head_t packed)
{
	fs_head_t unpacked;
	memcpy(&unpacked.ident,     &packed.ident,     sizeof(packed.ident));
	memcpy(&unpacked.sectors,   &packed.sectors,   sizeof(packed.sectors));
	memcpy(&unpacked.sreserved, &packed.sreserved, sizeof(packed.sreserved));
	memcpy(&unpacked.sdbmp,     &packed.sdbmp,     sizeof(packed.sdbmp));
	memcpy(&unpacked.sibmp,     &packed.sibmp,     sizeof(packed.sibmp));
	memcpy(&unpacked.sinodes,   &packed.sinodes,   sizeof(packed.sinodes));
	return unpacked;
}

pack_fs_inode_t
pack_inode(fs_inode_t unpacked)
{
	pack_fs_inode_t packed;
	memcpy(&packed.type,   &unpacked.type,   sizeof(packed.type));
	memcpy(&packed.sector, &unpacked.sector, sizeof(packed.sector));
	memcpy(&packed.length, &unpacked.length, sizeof(packed.length));
	memcpy(&packed.dirent, &unpacked.dirent, sizeof(packed.dirent));
	memcpy(&packed.size,   &unpacked.size,   sizeof(packed.size));
	return packed;
}

fs_inode_t
unpack_inode(pack_fs_inode_t packed)
{
	fs_inode_t unpacked;
	memcpy(&unpacked.type,   &packed.type,   sizeof(packed.type));
	memcpy(&unpacked.sector, &packed.sector, sizeof(packed.sector));
	memcpy(&unpacked.length, &packed.length, sizeof(packed.length));
	memcpy(&unpacked.dirent, &packed.dirent, sizeof(packed.dirent));
	memcpy(&unpacked.size,   &packed.size,   sizeof(packed.size));
	return unpacked;
}

/*
 * currently it is useless to call pack_dirent.
 * this function was introduced looking forward
 * to the future versions of the ZENFS filesystem
 */
pack_fs_dirent_t
pack_dirent(fs_dirent_t unpacked)
{
	pack_fs_dirent_t packed;
	memcpy(&packed.name,  &unpacked.name,  sizeof(packed.name));
	memcpy(&packed.inodei, &unpacked.inodei, sizeof(packed.inodei));
	return packed;
}

/*
 * this function is almost useless, happily it adds a NUL
 * terminator in the name to make it more handy to work with
 */
fs_dirent_t
unpack_dirent(pack_fs_dirent_t packed)
{
	fs_dirent_t unpacked;
	memcpy(&unpacked.name,  &packed.name,  sizeof(packed.name));
	/* include nul terminator in end */
	unpacked.name[FS_NAME_MAX] = '\0';
	memcpy(&unpacked.inodei, &packed.inodei, sizeof(packed.inodei));
	return unpacked;
}
