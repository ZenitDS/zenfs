#include <sys/stat.h>

#include <errno.h>
#include <fuse.h>
#include <libgen.h>
#include <limits.h>
#include <memory.h>
#include <stdlib.h>

#include "zenfs.h"
#include "fs.h"

/*
 * all parameters beginning with '_' (underscore) mean
 * that they are not used in the function.
 */

int
op_getattr(const char *path, struct stat *st)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	st->st_blksize = SECTOR_SIZE;
	st->st_blocks = inode.length;
	if (ISDIR(inode.type)) {
		st->st_mode = S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO;
		st->st_nlink = 2;
	} else {
		st->st_mode = S_IFREG | S_IRWXU | S_IRWXG | S_IRWXO;
		st->st_nlink = 1;
		st->st_size = inode.size;
	}

	return 0;
}

int
op_mknod(const char *path, mode_t _mode, dev_t _dev)
{
	int i;
	char resolved[PATH_MAX];
	char *name, *dir;
	inodei_t index, new_index;
	fs_inode_t inode, new_inode;
	pack_fs_dirent_t *buf;
	fs_dirent_t dirent;
	pack_fs_dirent_t packed_dirent;

	if (fsrealpath(path, resolved) == -1)
		return -errno;

	if ((name = basename(resolved)) == NULL)
		return -errno;

	if ((dir = dirname(resolved)) == NULL)
		return -errno;

	/* parent inode */
	if (resolve_inode(&index, dir) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (!ISDIR(inode.type))
		return -ENOTDIR;

	if (inode.dirent == DIRENT_MAX)
		return -ENOSPC;

	buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
	if (buf == NULL)
		return -1;

	if (read_idata(	index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	for (i = 0; i < inode.dirent; ++i) {
		dirent = unpack_dirent(buf[i]);

		if (strncmp(name, (char*)dirent.name, FS_NAME_MAX) == 0) {
			free(buf);
			return -EEXIST;
		}
	}
	free(buf);
	/* --------- */

	/* new file */
	new_inode.type = IT_FILE;
	new_inode.sector = 0;
	new_inode.length = 0;
	new_inode.dirent = 89; /* a cool but useless number :) */
	new_inode.size = 0;

	if (find_free_inode(&new_index) == -1)
		return -errno;

	if (write_inode(new_index, new_inode) == -1)
		return -errno;
	/* --------- */

	/* parent dir */
	++inode.dirent;
	if (write_inode(index, inode) == -1)
		return -errno;

	inode.length = idivup(inode.dirent * sizeof(pack_fs_dirent_t), SECTOR_SIZE);
	if (ensure_ilength(index, inode.length) == -1)
		return -errno;

	(void)memcpy(dirent.name, name, sizeof(dirent.name));
	dirent.inodei = new_index;

	packed_dirent = pack_dirent(dirent);

	if (write_idata(index, &packed_dirent,
			sizeof(pack_fs_dirent_t),
			(inode.dirent-1) * sizeof(pack_fs_dirent_t)) == -1)
		return -errno;
	/* --------- */

	return 0;
}

int
op_mkdir(const char *path, mode_t _mode)
{
	int i;
	char resolved[PATH_MAX];
	char *name, *dir;
	inodei_t index, new_index;
	fs_inode_t inode, new_inode;
	pack_fs_dirent_t *buf;
	fs_dirent_t dirents[2];
	pack_fs_dirent_t packed_dirents[2];

	/* get path info */
	if (fsrealpath(path, resolved) == -1)
		return -errno;

	if ((name = basename(resolved)) == NULL)
		return -errno;

	if ((dir = dirname(resolved)) == NULL)
		return -errno;
	/* ------------- */

	/* parent inode */
	if (resolve_inode(&index, dir) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (!ISDIR(inode.type))
		return -ENOTDIR;

	if (inode.dirent == DIRENT_MAX)
		return -ENOSPC;

	buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
	if (buf == NULL)
		return -1;

	if (read_idata(	index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	for (i = 0; i < inode.dirent; ++i) {
		dirents[0] = unpack_dirent(buf[i]);

		if (strncmp(name, (char*)dirents[0].name, FS_NAME_MAX) == 0) {
			free(buf);
			return -EEXIST;
		}
	}
	free(buf);
	/* -------------- */

	/* new dir */
	new_inode.type = IT_DIR;
	new_inode.sector = 0;
	new_inode.length = 0;
	new_inode.dirent = 2;
	new_inode.size = 0;

	if (find_free_inode(&new_index) == -1)
		return -errno;

	if (write_inode(new_index, new_inode) == -1)
		return -errno;

	if (ensure_ilength(new_index, 1) == -1)
		return -errno;

	(void)strcpy((char*)dirents[0].name, ".");
	dirents[0].inodei = new_index;
	(void)strcpy((char*)dirents[1].name, "..");
	dirents[1].inodei = index;

	packed_dirents[0] = pack_dirent(dirents[0]);
	packed_dirents[1] = pack_dirent(dirents[1]);

	if (write_idata(new_index, packed_dirents, sizeof(packed_dirents), 0) == -1)
		return -errno;
	/* --------- */

	/* parent dir */
	++inode.dirent;
	inode.length = idivup(inode.dirent * sizeof(pack_fs_dirent_t), SECTOR_SIZE);
	if (write_inode(index, inode) == -1)
		return -errno;

	if (ensure_ilength(index, inode.length) == -1)
		return -errno;

	(void)memcpy(dirents[0].name, name, sizeof(dirents[0].name));
	dirents[0].inodei = new_index;

	packed_dirents[0] = pack_dirent(dirents[0]);

	if (write_idata(index, &packed_dirents[0], sizeof(pack_fs_dirent_t),
			(inode.dirent-1) * sizeof(pack_fs_dirent_t)) == -1)
		return -errno;
	/* ---------- */

	return 0;
}

int
op_open(const char *path, struct fuse_file_info *_ffi)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (ISDIR(inode.type))
		return -EISDIR;

	return 0;
}

int
op_read(const char *path, char *buf,
		size_t nbytes, off_t off,
		struct fuse_file_info *_ffi)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (ISDIR(inode.type))
		return -EISDIR;

	if (off <= inode.size) {
		if (off + nbytes > inode.size)
			nbytes = inode.size - off;
		if (read_idata(index, buf, nbytes, off) == -1)
			return -errno;
	} else {
		return -EINVAL;
	}

	return nbytes;
}

int
op_readdir(const char *path, void *data,
		fuse_fill_dir_t filler, off_t off,
		struct fuse_file_info *_ffi)
{
	int i;
	inodei_t index;
	fs_inode_t inode;
	fs_dirent_t dirent;
	pack_fs_dirent_t *buf;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (!ISDIR(inode.type))
		return -ENOTDIR;

	buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
	if (buf == NULL)
		return -1;

	if (read_idata(	index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	for (i = 0; i < inode.dirent; ++i) {
		dirent = unpack_dirent(buf[i]);
		filler(data, (char*)dirent.name, NULL, 0);
	}

	free(buf);

	return 0;
}

int
op_readlink(const char *path, char *buf, size_t bufsiz)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (ISDIR(inode.type))
		return -EISDIR;

	if (inode.size > bufsiz)
		inode.size = bufsiz;

	if (read_idata(index, buf, inode.size, 0) == -1)
		return -errno;

	return inode.size;
}

int
op_write(const char *path, const char *data,
		size_t nbytes, off_t off,
		struct fuse_file_info *_ffi)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (ISDIR(inode.type))
		return -EISDIR;

	if (off > inode.size)
		return -EINVAL;

	if (ensure_ilength(index, idivup(off + nbytes, SECTOR_SIZE)) == -1)
		return -errno;

	if (write_idata(index, (void*)data, nbytes, off) == -1)
		return -errno;

	return nbytes;
}

int
op_truncate(const char *path, off_t off)
{
	inodei_t index;
	fs_inode_t inode;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (ISDIR(inode.type))
		return -EISDIR;
	
	if (ensure_ilength(index, idivup(off, SECTOR_SIZE)) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;
	
	inode.size = off;

	if (write_inode(index, inode) == -1)
		return -1;

	return 0;
}

int
op_rmdir(const char *path)
{
	char resolved[PATH_MAX];
	char *name, *dir;
	inodei_t index;
	fs_inode_t inode, rminode;
	pack_fs_dirent_t *buf;
	fs_dirent_t dirent;
	int ndirent, i;

	if (resolve_inode(&index, path) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;
	if (!ISDIR(inode.type))
		return -ENOTDIR;
	if (inode.dirent > 2)
		return -ENOTEMPTY;

	if (fsrealpath(path, resolved) == -1)
		return -errno;

	if ((name = basename(resolved)) == NULL)
		return -errno;

	if ((dir = dirname(resolved)) == NULL)
		return -errno;

	if (resolve_inode(&index, dir) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (!ISDIR(inode.type))
		return -ENOTDIR;

	buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
	if (buf == NULL)
		return -1;

	if (read_idata(index, buf, inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	ndirent = -1;
	for (i = 0; i < inode.dirent; ++i) {
		dirent = unpack_dirent(buf[i]);

		if (strncmp(name, (char*)dirent.name, FS_NAME_MAX) == 0) {
			ndirent = i;
			break;
		}
	}

	if (ndirent == -1) {
		free(buf);
		return -ENOENT;
	}

	dirent = unpack_dirent(buf[ndirent]);
	if (read_inode(dirent.inodei, &rminode) == -1) {
		free(buf);
		return -errno;
	}

	if (!ISDIR(rminode.type)) {
		free(buf);
		return -ENOTDIR;
	}

	if (rminode.length > 0) {
		if (mark_sectors(rminode.sector, rminode.length, 0) == -1) {
			free(buf);
			return -errno;
		}
	}

	if (mark_inode(dirent.inodei, 0) == -1) {
		free(buf);
		return -errno;
	}

	buf[ndirent] = buf[inode.dirent-1];
	--inode.dirent;

	if (write_idata(index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	free(buf);

	if (write_inode(index, inode) == -1)
		return -errno;

	return 0;
}

int
op_unlink(const char *path)
{
	char resolved[PATH_MAX];
	char *name, *dir;
	inodei_t index;
	fs_inode_t inode, rminode;
	pack_fs_dirent_t *buf;
	fs_dirent_t dirent;
	int ndirent, i;

	if (fsrealpath(path, resolved) == -1)
		return -1;

	if ((name = basename(resolved)) == NULL)
		return -errno;

	if ((dir = dirname(resolved)) == NULL)
		return -errno;

	if (resolve_inode(&index, dir) == -1)
		return -errno;

	if (read_inode(index, &inode) == -1)
		return -errno;

	if (!ISDIR(inode.type))
		return -ENOTDIR;

	buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
	if (buf == NULL)
		return -1;

	if (read_idata(	index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	ndirent = -1;
	for (i = 0; i < inode.dirent; ++i) {
		dirent = unpack_dirent(buf[i]);

		if (strncmp(name, (char*)dirent.name, FS_NAME_MAX) == 0) {
			ndirent = i;
			break;
		}
	}

	if (ndirent == -1) {
		free(buf);
		return -ENOENT;
	}

	dirent = unpack_dirent(buf[ndirent]);
	if (read_inode(dirent.inodei, &rminode) == -1) {
		free(buf);
		return -errno;
	}

	if (ISDIR(rminode.type)) {
		free(buf);
		return -EISDIR;
	}

	if (rminode.length > 0) {
		if (mark_sectors(rminode.sector, rminode.length, 0) == -1) {
			free(buf);
			return -errno;
		}
	}

	if (mark_inode(dirent.inodei, 0) == -1) {
		free(buf);
		return -errno;
	}

	buf[ndirent] = buf[inode.dirent-1];
	--inode.dirent;
	inode.size -= sizeof(pack_fs_dirent_t);

	if (write_inode(index, inode) == -1) {
		free(buf);
		return -errno;
	}

	if (write_idata(index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
		free(buf);
		return -errno;
	}

	free(buf);

	return 0;
}

int
op_create(const char *path, mode_t mode, struct fuse_file_info *_ffi)
{
	return op_mknod(path, mode, 0);
}
