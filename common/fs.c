#include <ctype.h>
#include <errno.h>
#include <fuse.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "zenfs.h"
#include "fs.h"

static int fd;			/* file descriptor */
static fs_head_t header;	/* file system header */

/*
 * all functions in this file print a brief message,
 * set errno and return -1 in case of an error;
 * else they return 0.
 * output is returned via the pointer arguments when necessary.
 */

int
fssetup(const char *const path)
{
	pack_fs_head_t packed_header;
	ssize_t nread;

	/* open special and read header */
	fd = open(path, O_RDWR);
	if (fd == -1)
		return -1;

	errno = EIO;
	nread = pread(fd, &packed_header, sizeof(packed_header), PADDING);
	if (nread < sizeof(packed_header))
		return -1;

	header = unpack_head(packed_header);

	if (memcmp((void*)header.ident, "zenfs",
				sizeof(header.ident)) != 0) {
		errno = EIO;
		return -1;
	}

	return 0;
}

int
fsexit(void)
{
	if (close(fd) == -1)
		return -1;

	return 0;
}

/*
 * division rounding up
 */
int
idivup(int a, int b)
{
	div_t d;
	d = div(a, b);
	return d.quot + (d.rem ? 1 : 0);
}

/*
 * Checks the validity of an inodei_t index.
 * Does not check if it is set or not, just if it
 * would be valid. The implementation relies on this.
 */
int
check_index(inodei_t index)
{
	if (index > HEAD_IMAX(header)) {
		errno = EDOM;
		return -1;
	}

	return 0;
}

/*
 * ensure inode has at least len sectors
 */
int
ensure_ilength(inodei_t index, uint16_t len)
{
	fs_inode_t inode;
	void *buf;
	size_t bufalloc;

	if (check_index(index) == -1)
		return -1;

	if (read_inode(index, &inode) == -1)
		return -1;

	if (inode.length >= len)
		return 0;

	bufalloc = inode.size;
	buf = malloc(bufalloc);
	if (buf == NULL)
		return -1;

	if (bufalloc > 0)
		if (read_idata(	index, buf,
				inode.size, 0) == -1) {
			free(buf);
			return -1;
		}

	if (mark_sectors(inode.sector, inode.length, 0) == -1) {
		free(buf);
		return -1;
	}

	inode.length = len;

	if (find_sector_chain(&inode.sector, inode.length) == -1) {
		free(buf);
		return -1;
	}

	if (mark_sectors(inode.sector, inode.length, 1) == -1) {
		free(buf);
		return -1;
	}

	if (write_inode(index, inode) == -1) {
		free(buf);
		return -1;
	}

	if (bufalloc > 0)
		if (write_idata(index, buf, inode.size, 0) == -1) {
			free(buf);
			return -1;
		}

	free(buf);

	return 0;
}

int
find_free_inode(inodei_t *index)
{
	uint8_t buf[SECTOR_SIZE];
	int i, j, k;
	ssize_t nread;

	if (index == NULL) {
		errno = EINVAL;
		return -1;
	}

	for (i = 0; i < header.sibmp; ++i) {
		errno = EIO;
		nread = pread(fd, buf, sizeof(buf),
				(HEAD_IBMP_OFF(header) + i) * SECTOR_SIZE);
		if (nread < sizeof(buf))
			return -1;

		for (j = 0; j < SECTOR_SIZE; ++j)
			for (k = 0; k < 8; ++k)
				if (!(buf[j] & ((uint8_t)1 << k))) {
					*index = (i * SECTOR_SIZE + j) * 8 + k;
					return 0;
				}
	}

	errno = ENOSPC;
	return -1;
}

int
find_sector_chain(uint32_t *sector, uint16_t len)
{
	uint8_t buf[SECTOR_SIZE];
	int current, i, j, k;
	ssize_t nread;

	if (sector == NULL) {
		errno = EINVAL;
		return -1;
	}

	current = 0;
	*sector = 0;
	for (i = 0; i < header.sdbmp; ++i) {
		errno = EIO;
		nread = pread(fd, buf, sizeof(buf),
			(HEAD_DBMP_OFF(header) + i) * SECTOR_SIZE);
		if (nread < sizeof(buf))
			return -1;

		for (j = 0; j < SECTOR_SIZE; ++j) {
			for (k = 0; k < 8; ++k) {
				if ((i * SECTOR_SIZE + j) * 8 + k
						>= header.sectors) {
					errno = ENOSPC;
					return -1;
				}

				if (!(buf[j] & ((uint8_t)1 << k)))
					++current;
				else {
					current = 0;
					*sector =
						(i * SECTOR_SIZE + j) * 8 + k + 1;
				}

				if (current >= len)
					return 0;
			}
		}
	}

	errno = ENOSPC;
	return -1;
}

/*
 * makes cleanup of path and copies it into resolved.
 * resolved must be capable of holding FS_PATH_MAX characters.
 */
int
fsrealpath(const char *path, char *resolved)
{
	int cnt, i;

	if (path == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (resolved == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (strlen(path) >= FS_PATH_MAX)
		return -1;

	(void)strncpy(resolved, path, FS_PATH_MAX);

	if (resolved[0] != '/') {
		errno = EINVAL;
		return -1;
	}

	cnt = 0;
	for (i = 0; i < FS_PATH_MAX; ++i) {
		if (resolved[i] == '\0') break;
		if (resolved[i] == '/') {
			cnt = 0;
			/* remove extra '/' characters */
			while (i+1 < FS_PATH_MAX && resolved[i+1] == '/')
				(void)memmove(
					(void*)(resolved + i),
					(void*)(resolved + i + 1),
					strnlen(&resolved[i+1], FS_PATH_MAX)+1);
		} else
			++cnt;

		if (!isprint(resolved[i])) {
			errno = EINVAL;
			return -1;
		}

		if (cnt > FS_NAME_MAX) {
			errno = ENAMETOOLONG;
			return -1;
		}
	}

	/* erase trailing '/' characters excluding root */
	for (cnt = strlen(resolved)-1; cnt > 0 && resolved[cnt] == '/'; --cnt)
		resolved[cnt] = '\0';

	return 0;
}

int
inode_file_len(inodei_t index, size_t *size)
{
	fs_inode_t inode;

	if (check_index(index) == -1)
		return -1;

	if (size == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (read_inode(index, &inode) == -1)
		return -1;

	if (ISDIR(inode.type)) {
		errno = EISDIR;
		return -1;
	}

	*size = inode.size;

	return 0;
}

/*
 * DESC
 * Mark inode with bit set
 * RET
 * S 0
 * E -1, errno
 */
int
mark_inode(inodei_t index, int set)
{
	uint8_t byte, mask;
	ssize_t nwrite, nread;

	if (check_index(index) == -1)
		return -1;

	if (set != 0 && set != 1) {
		errno = EINVAL;
		return -1;
	}

	errno = EIO;
	nread = pread(fd, &byte, sizeof(byte),
			HEAD_IBMP_OFF(header) * SECTOR_SIZE + index / 8);
	if (nread < sizeof(byte))
		return -1;

	mask = (uint8_t)1 << index % 8;
	byte = (set) ? (byte | mask) : (byte & ~mask);
	
	errno = EIO;
	nwrite = pwrite(fd, &byte, sizeof(byte),
			HEAD_IBMP_OFF(header) * SECTOR_SIZE + index / 8);
	if (nwrite < sizeof(byte))
		return -1;

	return 0;
}

/*
 * DESC
 * Set 'len' sectors starting from 'start'
 * in bitmap as used/unused, as in mark_inode but for sectors
 * RET
 * S 0
 * E -1, errno
 */
int
mark_sectors(uint32_t start, uint16_t len, int set)
{
	size_t bufcnt;
	uint8_t *buf, mask;
	int i;
	ssize_t nread, nwrite;

	if (len == 0)
		return 0;

	if (start + len > header.sectors) {
		errno = EDOM;
		return -1;
	}

	if (set != 0 && set != 1) {
		errno = EINVAL;
		return -1;
	}

	bufcnt = idivup(start+len, 8) - start / 8;
	buf = malloc(bufcnt * sizeof(*buf));
	if (buf == NULL)
		return -1;

	/*
	 * even though it is inefficient to do 1 byte at a time it is
	 * much simpler and this function is not expected to handle
	 * large amounts of sectors
	 */
	errno = EIO;
	nread = pread(fd, buf, bufcnt * sizeof(*buf),
		HEAD_DBMP_OFF(header) * SECTOR_SIZE + start / 8);
	if (nread < bufcnt * sizeof(*buf)) {
		free(buf);
		return -1;
	}

	if (bufcnt > 1) {
		mask = (uint8_t)-1 << start % 8;
		buf[0] = (set) ? (buf[0] | mask) : (buf[0] & ~mask);

		mask = (uint8_t)-1;
		for (i = 1; i < bufcnt-1; ++i) {
			buf[i] = (set) ? (buf[0] | mask) : (buf[0] & ~mask);
		}

	   	if ((start + len) % 8 != 0)
			mask = (uint8_t)-1 >> (8 - (start + len) % 8);
		else
			mask = (uint8_t)-1;

		buf[bufcnt-1] = (set) ?
				(buf[bufcnt-1] | mask) :
				(buf[bufcnt-1] & ~mask);
	} else {
		mask = (uint8_t)-1 << start % 8;
		if ((start + len) % 8 != 0)
			mask &= (uint8_t)-1 >> (8 - (start + len) % 8);

		buf[0] = (set) ? (buf[0] | mask) : (buf[0] & ~mask);
	}

	errno = EIO;
	nwrite = pwrite(fd, buf, bufcnt * sizeof(*buf),
			HEAD_DBMP_OFF(header) * SECTOR_SIZE + start / 8);
	if (nwrite < bufcnt * sizeof(*buf)) {
		free(buf);
		return -1;
	}

	free(buf);

	return 0;
}

/*
 * read inode data
 * buf must be large enough to hold nbytes
 * if error { set_errno; return -1; } else return 0;
 */
int
read_idata(inodei_t index, void *buf, size_t nbytes, off_t offset)
{
	fs_inode_t inode;
	ssize_t nread;

	if (check_index(index) == -1)
		return -1;

	if (buf == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (read_inode(index, &inode))
		return -1;

	if (nbytes + offset > inode.size) {
		errno = EDOM;
		return -1;
	}
	errno = EIO;
	nread = pread(fd, buf, nbytes, inode.sector * SECTOR_SIZE + offset);
	if (nread < nbytes)
		return -1;

	return 0;
}

/*
 * read inode at index to dest
 * if error { set_errno; return -1; } else return 0;
 */
int
read_inode(inodei_t index, fs_inode_t *dest)
{
	pack_fs_inode_t packed_inode;

	if (check_index(index) == -1)
		return -1;

	if (dest == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (pread(fd, &packed_inode, sizeof(pack_fs_inode_t),
			HEAD_I_OFF(header) * SECTOR_SIZE +
			index * sizeof(pack_fs_inode_t)) == -1)
		return -1;

	*dest = unpack_inode(packed_inode);

	return 0;
}

/*
 * get inode corresponding to path
 * if error { set_errno; return -1; } else return 0;
 */
int
resolve_inode(inodei_t *dest, const char *path)
{
	int i;
	char resolved[FS_PATH_MAX], *it_resolved;
	size_t spn;
	pack_fs_dirent_t *buf;
	fs_dirent_t dirent;
	fs_inode_t inode;
	inodei_t index;

	if (dest == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (path == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (strnlen(path, FS_PATH_MAX+1) > FS_PATH_MAX) {
		errno = ENAMETOOLONG;
		return -1;
	}

	if (fsrealpath(path, resolved) == -1)
		return -1;

	index = 0;
	it_resolved = resolved + 1;
	for (;;) {
		if (*it_resolved == '\0') break;
		if (read_inode(index, &inode) == -1)
			return -1;

		if (!ISDIR(inode.type)) {
			errno = ENOTDIR;
			return -1;
		}

		buf = malloc(inode.dirent * sizeof(pack_fs_dirent_t));
		if (buf == NULL)
			return -1;

		if (read_idata(index, buf,
			inode.dirent * sizeof(pack_fs_dirent_t), 0) == -1) {
			free(buf);
			return -1;
		}

		spn = strcspn(it_resolved, "/");
		if (spn >= FS_NAME_MAX) {
			errno = ENAMETOOLONG;
			free(buf);
			return -1;
		}

		for (i = 0; i < inode.dirent; ++i) {
			dirent = unpack_dirent(buf[i]);

			if (strnlen((char*)dirent.name, FS_NAME_MAX) == spn &&
				strncmp((char*)dirent.name,
					it_resolved, spn) == 0) {
				index = dirent.inodei;
				break;
			}
		}

		free(buf);

		if (i == inode.dirent) {
			errno = ENOENT;
			return -1;
		}

		it_resolved = strchr(it_resolved, '/');
		if (it_resolved == NULL) break;
		++it_resolved;
	}

	*dest = index;

	return 0;
}

int
write_idata(inodei_t index, void *buf, size_t nbytes, off_t offset)
{
	fs_inode_t inode;
	ssize_t nwrite;

	if (check_index(index) == -1)
		return -1;

	if (buf == NULL) {
		errno = EINVAL;
		return -1;
	}

	if (read_inode(index, &inode))
		return -1;

	if (nbytes + offset > inode.length * SECTOR_SIZE) {
		errno = EDOM;
		return -1;
	}

	errno = EIO;
	nwrite = pwrite(fd, buf, nbytes, inode.sector * SECTOR_SIZE + offset);
	if (nwrite < nbytes)
		return -1;

	if (inode.size < nbytes + offset)
		inode.size = nbytes + offset;
	
	if (write_inode(index, inode) == -1)
		return -1;

	return 0;
}

int
write_inode(inodei_t index, fs_inode_t inode)
{
	pack_fs_inode_t packed_inode;
	ssize_t nwrite;

	if (check_index(index) == -1)
		return -1;

	packed_inode = pack_inode(inode);
	errno = EIO;
	nwrite = pwrite(fd, &packed_inode, sizeof(packed_inode),
			HEAD_I_OFF(header) * SECTOR_SIZE +
			index * sizeof(pack_fs_inode_t));
	if (nwrite < sizeof(packed_inode))
		return -1;

	if (mark_inode(index, 1) == -1)
		return -1;

	return 0;
}
