#ifndef _COMMON_OP_H
#define _COMMON_OP_H

#include <fuse.h>

int op_create(const char*, mode_t, struct fuse_file_info*);
int op_getattr(const char*, struct stat*);
int op_mknod(const char*, mode_t, dev_t);
int op_mkdir(const char*, mode_t);
int op_open(const char*, struct fuse_file_info*);
int op_read(const char*, char*, size_t, off_t, struct fuse_file_info*);
int op_readdir(const char*, void *, fuse_fill_dir_t, off_t, struct fuse_file_info*);
int op_rmdir(const char*);
int op_truncate(const char*, off_t);
int op_unlink(const char*);
int op_write(const char*, const char*, size_t, off_t, struct fuse_file_info*);

#endif
