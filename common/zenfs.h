#ifndef _ZENFS_H
#define _ZENFS_H

#include <stdint.h>

/*
 * pack_*: packed version of data
 * (needed to prevent padding introduced by C)
 * pack_*() and unpack_*() can be used
 * to translate from unpacked to packed and reverse
 */

/* header padding from start of disk */
#define PADDING ((uint32_t)3)
#define SECTOR_SIZE ((uint32_t)512)
/* maximum length of pathname */
#define FS_NAME_MAX ((uint32_t)30)
/* maximum length of path (including NUL termination) */
#define FS_PATH_MAX ((uint32_t)1024)
/* maximum amount of directory entries */
#define DIRENT_MAX ((uint8_t)256)

/* offsets (in sectors)  */
/* data bitmap offset */
#define HEAD_DBMP_OFF(H) ((uint32_t)(H).sreserved)
/* inode bitmap offset */
#define HEAD_IBMP_OFF(H) ((uint32_t)((H).sreserved + (H).sdbmp))
/* inode data offset */
#define HEAD_I_OFF(H) ((uint32_t)((H).sreserved + (H).sdbmp + (H).sibmp))
/* data offset */
#define HEAD_D_OFF(H) ((uint32_t)((H).sreserved + (H).sdbmp + (H).sibmp + (H).sinodes))

/* max value of inode index */
#define HEAD_IMAX(H) ((uint32_t)((H).sinodes * SECTOR_SIZE / sizeof(pack_fs_inode_t) - 1))

/* inode.type */
#define IT_DIR ((uint32_t)0x1)
#define IT_FILE ((uint32_t)0x2)
#define ISDIR(IT)  ((uint32_t)(IT) & IT_DIR)
#define ISFILE(IT) ((uint32_t)(IT) & IT_FILE)

typedef uint16_t inodei_t;		/* inode index type */

/* == unpacked == */
/* filesystem header (padded by PADDING) */
typedef struct {
	uint8_t ident[5];		/* identifier */

	uint32_t sectors;		/* number of sectors >=4 */
	uint16_t sreserved;		/* number of reserved sectors >=1 */
	uint16_t sdbmp;			/* number of sectors for data bitmap >=1 */
	uint16_t sibmp;			/* number of sectors for inode bitmap >=1 */
	uint16_t sinodes;		/* number of sectors for inodes >=1 */
} fs_head_t;

/* inode structure */
/*
 * IMPORTANT! the following must be satisfied
 * sizeof(fs_inode_t) | SECTOR_SIZE
 * sizeof(fs_dirent_t) | SECTOR_SIZE
 * <look at divisibility of integers>
 */
typedef struct {
	uint8_t type;			/* type (file/directory) */
	uint32_t sector;		/* offset of first sector (LBA) */
	uint16_t length;		/* number of sectors */
	uint8_t dirent;			/* number of directory entries */
	uint32_t size;			/* file size in bytes */
} fs_inode_t;

typedef struct {
	uint8_t name[FS_NAME_MAX + 1];	/* the last one for unpack null terminator */
	inodei_t inodei;		/* inode index */
} fs_dirent_t;

/* == packed == */
typedef struct {
	uint8_t ident[5];

	uint8_t sectors[4];
	uint8_t sreserved[2];
	uint8_t sdbmp[2];
	uint8_t sibmp[2];
	uint8_t sinodes[2];
} pack_fs_head_t;

/*
 * IMPORTANT! the following must be satisfied
 * SECTOR_SIZE | sizeof(pack_fs_inode_t)
 * SECTOR_SIZE | sizeof(pack_fs_dirent_t)
 * <look at congruency of integers>
 */
typedef struct {
	uint8_t type;
	uint8_t sector[4];
	uint8_t length[2];
	uint8_t dirent;
	uint8_t size[4];
	uint8_t padding[4];
} pack_fs_inode_t;

typedef struct {
	uint8_t name[FS_NAME_MAX];
	uint8_t inodei[2];
} pack_fs_dirent_t;

/* zenfs.c */
/*
 * utility functions to translate from
 * packed to unpacked with ease.
 */
pack_fs_head_t pack_head(fs_head_t);
pack_fs_inode_t pack_inode(fs_inode_t);
pack_fs_dirent_t pack_dirent(fs_dirent_t);
fs_head_t unpack_head(pack_fs_head_t);
fs_inode_t unpack_inode(pack_fs_inode_t);
fs_dirent_t unpack_dirent(pack_fs_dirent_t);

#endif
