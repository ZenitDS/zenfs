#include <err.h>
#include <fcntl.h>
#include <inttypes.h>
#include <limits.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../common/zenfs.h"

static int idivup(int, int);

static int format_fs(int);
static void usage(void);

static int Fflag, Vflag;
static fs_head_t header;

/*
 * options:
 * 	-f: only format the header and dont touch anything more
 * 	-i: number of inode sectors
 * 	-r: number of reserved sectors
 * 	-v: verbose (very useless)
 */

int
main(int argc, char *const *argv)
{
	int ch, fd, i;
	char path[PATH_MAX]; /* special path */

	(void)memcpy(header.ident, "zenfs", sizeof(header.ident));
	header.sreserved = 1;
	header.sinodes = 1;
	
	Fflag = Vflag = 0;
	while ((ch = getopt(argc, argv, "fi:r:v")) != -1) {
		switch (ch) {
		case 'f':
			Fflag = 1;
			break;
		case 'i':
			if (sscanf(optarg, "%512" SCNu16 "", &header.sinodes) < 1)
				errx(1, "(-i) cannot parse %512s as uint16_t",
					optarg);
			if (header.sinodes < 1)
				errx(1, "you know you want at least 1"
					"inode sector, dont you?");
			break;
		case 'r':
			if (sscanf(optarg, "%512" SCNu16 "", &header.sreserved) < 1)
				errx(1, "(-r) cannot parse %512s as uint16_t",
					optarg);
			if (header.sreserved < 1)
				errx(1, "you know you want at least 1"
					"reserved sector, dont you?");
			break;
		case 'v':
			Vflag = 1;
			break;
		default:
			usage();
		}
	}
	argv += optind;
	argc -= optind;

	if (argc < 1)
		usage();

	for (i = 0; i < argc; ++i) {
		if (realpath(argv[i], path) == NULL)
			err(1, "realpath %s", argv[i]);

		if ((fd = open(path, O_WRONLY)) == -1)
			err(1, "open %s", path);

		if (Vflag)
			printf("formatting %s\n", path);

		if (format_fs(fd) == -1)
			err(1, "format_fs");

		if (close(fd) == -1)
			err(1, "close");
	}
}

static int
format_fs(int fd)
{
	off_t off;
	uint32_t used_sectors, buf;
	pack_fs_head_t packed_header;
	pack_fs_inode_t packed_inode;
	pack_fs_dirent_t packed_dirents[2];

	fs_inode_t inode;
	fs_dirent_t unpacked_dirent;

	/* store size of disk in 'off' */
	if ((off = lseek(fd, 0, SEEK_END)) == -1) {
		warn("lseek");
		return -1;
	}

	header.sectors = (off / SECTOR_SIZE > UINT_MAX) ? 
			UINT_MAX : 
			off / SECTOR_SIZE;
	header.sdbmp = idivup(header.sectors, SECTOR_SIZE*8);
	header.sibmp = idivup(header.sinodes, SECTOR_SIZE*8);

	if (Vflag) {
		(void)printf("\theader.ident: %5s\n", header.ident);
		(void)printf("\theader.sectors: %" 	PRIu32 "\n", header.sectors);
		(void)printf("\theader.sreserved: %" 	PRIu16 "\n", header.sreserved);
		(void)printf("\theader.sdbmp: %" 	PRIu16 "\n", header.sdbmp);
		(void)printf("\theader.sibmp: %" 	PRIu16 "\n", header.sibmp);
		(void)printf("\theader.sinodes: %" 	PRIu16 "\n", header.sinodes);
	}

	/* include one last sector for the root data */
#define MIN_SECTORS \
	(header.sreserved + header.sdbmp + header.sibmp + header.sinodes + 1)

	if (header.sectors < MIN_SECTORS) {
		warn("file is too small for %d sectors.", MIN_SECTORS);
		return -1;
	}

	packed_header = pack_head(header);
	if (pwrite(fd, &packed_header, sizeof(pack_fs_head_t), PADDING) == -1) {
		warn("pwrite");
		return -1;
	}


	if (Fflag) /* only format header */
		return 0;

	if (Vflag)
		printf("writing bitmaps\n");

	/*
	 * map the data bitmap
	 */
	if (lseek(fd, HEAD_DBMP_OFF(header) * SECTOR_SIZE, SEEK_SET) == -1) {
		warn("lseek");
		return -1;
	}

	/* start writing blocks of 32 bits */
	buf = -1;
	for (used_sectors = MIN_SECTORS; used_sectors >= 32; used_sectors -= 32) {
		if (write(fd, &buf, sizeof(buf)) == -1) {
			warn("write");
			return -1;
		}
	}

	/* write rest of bitmap */
	if (used_sectors > 0) {
		buf >>= 32 - used_sectors; 
		if (write(fd, &buf, sizeof(buf)) == -1) {
			warn("write");
			return -1;
		}
	}

	/*
	 * map the inode bitmap
	 * - just map the root inode, which is required
	 */
	buf = 1;
	if (pwrite(	fd, &buf, sizeof(buf),
			HEAD_IBMP_OFF(header) * SECTOR_SIZE) == -1) {
		warn("pwrite");
		return -1;
	}

	/*
	 * map the root inode
	 */
	if (Vflag)
		printf("mapping root inode\n");
	inode.type = IT_DIR;
	inode.sector = HEAD_D_OFF(header);
	inode.length = 1;
	inode.dirent = 2;
	inode.size = sizeof(pack_fs_dirent_t) * inode.dirent;
	packed_inode = pack_inode(inode);
	if (pwrite(fd, &packed_inode, sizeof(pack_fs_inode_t), 
				HEAD_I_OFF(header) * SECTOR_SIZE) == -1) {
		warn("pwrite");
		return -1;
	}

	/* map the root data */
	if (Vflag)
		printf("creating root directory entries\n");

	strcpy((char*)unpacked_dirent.name, ".");
	unpacked_dirent.inodei = 0;
	packed_dirents[0] = pack_dirent(unpacked_dirent);

	strcpy((char*)unpacked_dirent.name, "..");
	unpacked_dirent.inodei = 0;
	packed_dirents[1] = pack_dirent(unpacked_dirent);

	if (pwrite(fd, packed_dirents, sizeof(packed_dirents), 
				HEAD_D_OFF(header) * SECTOR_SIZE) == -1) {
		warn("pwrite");
		return -1;
	}

	return 0;
}

static void
usage(void)
{
	(void)fputs("usage: newfs_zenfs [-fv] [-r reserved-sectors] [-i inode-sectors] special ...\n", stderr);
	exit(1);
}

int
idivup(int a, int b)
{
	div_t d;
	d = div(a, b);
	return d.quot + (d.rem ? 1 : 0);
}
