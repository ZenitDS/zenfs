#!/bin/ksh

# File consistency checks with some file movement
# in hope that some issues are catched, fingers crossed.

FILE=$(mktemp)
FILE1=$(mktemp);
FILE2=$(mktemp);
FILE3=$(mktemp);
FILE4=$(mktemp);
DIR=$(mktemp -d);

dd if=/dev/zero of=$FILE bs=512 count=16383
dd if=/dev/random of=$FILE1 bs=512 count=1024
dd if=/dev/random of=$FILE2 bs=512 count=2047
dd if=/dev/random of=$FILE3 bs=512 count=1024
dd if=/dev/random of=$FILE4 bs=512 count=4097

./newfs/newfs_zenfs -v $FILE

./mount/mount_zenfs -s $FILE $DIR

cp $FILE1 $DIR/file1
cp $FILE2 $DIR/file2
cp $FILE3 $DIR/file3

rm $DIR/file2
rm $DIR/file1

cp $FILE4 $DIR/file4

cp $DIR/file4 $DIR/file5
rm $DIR/file4
cp $DIR/file5 $DIR/file4

cp $FILE2 $DIR/file1
cp $FILE1 $DIR/file2

ls -lah $DIR

ERR=0
cmp -s $FILE2 $DIR/file1 || ERR=1
cmp -s $FILE1 $DIR/file2 || ERR=1
cmp -s $FILE3 $DIR/file3 || ERR=1
cmp -s $FILE4 $DIR/file4 || ERR=1
cmp -s $FILE4 $DIR/file5 || ERR=1

if [[ $ERR == 0 ]]
then
	echo "Files were consistent. The world can live one more day"
	echo "[SUCCESS] Tests passed successfully"
else
	echo "[FAILED] Files were inconsistent"
fi

umount $DIR

rm $FILE $FILE1 $FILE2 $FILE3 $FILE4

rmdir $DIR
