#include "cmd.h"

#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include "../common/op.h"

static int
_filldir_hook(	void *_0, const char *name, 
		const struct stat* _2, off_t _3)
{
	printf("%s ", name);

	return 0;
}

DEF(ls)
{
	int i;

	if (argc == 0) {
		op_readdir("/", NULL, _filldir_hook, 0, NULL);
		puts("");
	} else {
		for (i = 0; i < argc; ++i) {
			printf("=> %s\n", argv[0]);
			op_readdir(argv[i], NULL, _filldir_hook, 0, NULL);
			puts("");
		}
	}

	return NULL;
}

DEF(cp)
{
	struct stat stf, stw;
	uint8_t *buf;

	if (argc != 2)
		return "cp only supports two arguments";
	
	if (op_getattr(argv[0],  &stf) < 0)
		return "could not stat file";

	if (!S_ISREG(stf.st_mode))
		return "file is not regular";
	
	if (op_getattr(argv[1], &stw) < 0)
		if (op_mknod(argv[1], 0, 0) < 0)
			return "could not create file";

	if (op_getattr(argv[1], &stw) < 0)
		return "could not stat already created file";

	if (op_truncate(argv[1], stw.st_size) < 0)
		return "could not truncate file";
	
	buf = malloc(stf.st_size+1);
	if (!buf) 
		return "could not allocate memory";
	
	if (op_read(argv[0], (char*)buf, stf.st_size, 0, NULL) < 0) {
		free(buf);
		return "could not read file";
	}

	if (op_write(argv[1], (char*)buf, stf.st_size, 0, NULL) < 0) {
		free(buf);
		return "could not write file";
	}

	free(buf);
	
	return NULL;
}

DEF(oicp)
{
	int fd;
	struct stat stf, stw;
	uint8_t *buf;

	if (argc != 2)
		return "oicp only supports two arguments";
	
	if (stat(argv[0], &stf) < 0)
		return "could not stat file";

	if (!S_ISREG(stf.st_mode))
		return "file is not regular";
	
	if (op_getattr(argv[1], &stw) < 0)
		if (op_mknod(argv[1], 0, 0) < 0)
			return "could not create file";

	if (op_getattr(argv[1], &stw) < 0)
		return "could not stat already created file";

	if (op_truncate(argv[1], stf.st_size) < 0)
		return "could not truncate file";

	fd = open(argv[0], O_RDONLY);
	if (fd == -1)
		return "could not open file";
	
	buf = malloc(stf.st_size);
	if (!buf) {
		close(fd);
		return "could not allocate memory";
	}
	
	if (read(fd, (char*)buf, stf.st_size) < 0) {
		free(buf);
		close(fd);
		return "could not read file";
	}

	close(fd);

	if (op_write(argv[1], (char*)buf, stf.st_size, 0, NULL) < 0) {
		free(buf);
		return "could not write file";
	}

	free(buf);
	
	return NULL;
}

DEF(iocp)
{
	int fd;
	struct stat stf;
	uint8_t *buf;

	if (argc != 2)
		return "iocp only supports two arguments";
	
	if (op_getattr(argv[0], &stf) < 0)
		return "could not stat file";
	
	if (!S_ISREG(stf.st_mode))
		return "file is not regular";
	
	buf = malloc(stf.st_size);
	if (!buf)
		return "could not allocate memory";

	if (op_read(argv[0], (char*)buf, stf.st_size, 0, NULL) < 0) {
		free(buf);
		return "could not read file";
	}

	fd = open(argv[1], O_WRONLY | O_TRUNC | O_CREAT, ACCESSPERMS);
	if (fd == -1) {
		free(buf);
		return "could not open file";
	}

	if (write(fd, (char*)buf, stf.st_size) == -1) {
		close(fd);
		free(buf);
		return "could not write file";
	}

	close(fd);
	free(buf);
	
	return NULL;
}

DEF(mkdir)
{
	int i;
	for (i = 0; i < argc; ++i)
		(void)op_mkdir(argv[i], 0);

	return NULL;
}

DEF(rmdir)
{
	int i;
	for (i = 0; i < argc; ++i)
		(void)op_rmdir(argv[i]);
	
	return NULL;
}

DEF(mknod)
{
	int i;
	for (i = 0; i < argc; ++i)
		op_mknod(argv[i], 0, 0);
	return NULL;
}

DEF(unlink)
{
	int i;
	for (i = 0; i < argc; ++i)
		op_unlink(argv[i]);
	return NULL;
}
