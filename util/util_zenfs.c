#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <err.h>
#include <fcntl.h>

#include "../common/zenfs.h"
#include "../common/fs.h"

#include "cmd.h"

#define LEN(a) (sizeof(a) / sizeof(*a))

static void usage(void);

typedef struct {
	char *name;
	char *(*func)(int, const char**);
} COMMAND;

#define INS_COMMAND(__NAME) { #__NAME, c_##__NAME },
COMMAND commands[] = {
		INS_COMMAND(ls)
		INS_COMMAND(cp)
		INS_COMMAND(oicp)
		INS_COMMAND(iocp)
		INS_COMMAND(mkdir)
		INS_COMMAND(rmdir)
		INS_COMMAND(mknod)
		INS_COMMAND(unlink)
		{ NULL, NULL }
	};

int
main(int argc, const char **argv)
{
	int i;
	char *msg;
	if (argc <= 2)
		usage();
	
	if (fssetup(argv[1]) == -1)
		err(1, "fssetup");

	for (i = 0; commands[i].name; ++i) {
		if (strcmp(commands[i].name, argv[2]))
			continue;

		msg = (commands[i].func)(argc - 3, argv + 3);
		if (msg)
			warn("%s: %s", commands[i].name, msg);

		break;
	}

	if (!commands[i].name)
		warn("unknown command");

	if (fsexit() == -1)
		err(1, "fsexit");
}

static void
usage(void)
{

	fprintf(stderr, "usage: %s file command [args...]\n", getprogname());
	exit(EXIT_FAILURE);
}
