#ifndef _ZENFS_CMD_H
#define _ZENFS_CMD_H

#define DEF(__NAME) char *c_##__NAME(int argc, const char **argv) 

DEF(ls);
DEF(cp);
DEF(oicp);
DEF(iocp);
DEF(mkdir);
DEF(rmdir);
DEF(mknod);
DEF(unlink);

#endif
