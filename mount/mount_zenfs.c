#include <err.h>
#include <fuse.h>
#include <limits.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

#include "../common/zenfs.h"
#include "../common/fs.h"
#include "../common/op.h"

void usage(void);

int
main(int argc, char **argv)
{
	{
		int Sflag, i;
		char resolved[PATH_MAX];

		Sflag = 0;
		for (i = 1; i < argc && argv[i][0] == '-'; ++i) {
			if (argv[i][1] == 's') Sflag = 1;
		}

		/* require special, node, and -s flag */
		if (argc-i != 2 || !Sflag)
			usage();

		/* get path for special and erase it from argv */
		if (realpath(argv[i], resolved) == NULL)
			err(1, "realpath %.512s", argv[i]);
		(void)memmove(	&argv[i],
				&argv[i+1],
				(argc - (i+1)) * sizeof(argv[0]));
		--argc;

		if (fssetup(resolved) == -1)
			err(1, "fssetup");
	}

	{

		int out;
		struct fuse_operations op;

		memset(&op, 0, sizeof(op));
		op.create = op_create;
		op.getattr = op_getattr;
		op.mknod = op_mknod;
		op.mkdir = op_mkdir;
		op.open = op_open;
		op.read = op_read;
		op.readdir = op_readdir;
		op.rmdir = op_rmdir;
		op.truncate = op_truncate;
		op.unlink = op_unlink;
		op.write = op_write;

		out = fuse_main(argc, argv, &op, NULL);

		if (fsexit() == -1)
			err(1, "fsexit");
		return out;
	}
}

void
usage(void)
{
	(void)fputs("usage: mount_zenfs [fuse_flags(-s mandatory)] special node\n", stderr);
	exit(1);
}

